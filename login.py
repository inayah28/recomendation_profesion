from tkinter import *
from tkinter import ttk
from tkinter import messagebox as ms
import sqlite3
import csv
from module.modNormalize import normalize
from math import sqrt
from collections import Counter

class Login (object):

    
    
    def __init__(self, master):
        self.master = master
        self.master.title ('Sistem Rekomnedasi Profesi')
        self.npm = StringVar()
        self.nama = StringVar()
        self.alamat = StringVar()


        

        self.db_name = 'skripsi.db'

        

        Label (text = 'SILAHKAN LOGIN !',font = ('',25),pady = 15).grid (row = 0, column = 2)
        Label (text = 'NPM : ',font = ('',20)).grid (row = 1, column = 1)

        login_entry = ttk.Entry(textvariable = self.npm, font = ('',20))
        login_entry.grid (row = 1, column = 2)
        login_entry.focus()

        Button (text = 'LOGIN',bd = 2, font = ('',15), padx = 25, command = self.login).grid (row = 2, column = 2, sticky = W)
    

    def run_query (self, query, parameters = ()):
        with sqlite3.connect (self.db_name) as conn:
            cursor = conn.cursor()
            cursor.execute(query,parameters)
            query_result = cursor.fetchall()
            conn.commit ()
        return query_result

    def login(self):
        query = 'SELECT * FROM mahasiswa WHERE npm = ?'
        parameters = (self.npm.get(),)
        result = self.run_query (query, parameters)
        if result:

            nama = result[0][1]
            alamat = result[0][3]
            self.nama.set(nama)
            ms.showinfo('Sussces',self.nama.get() + '\nBerhasil Login')
            self.master.destroy()
            application = view(nama, self.npm.get(), alamat)

        else:
            ms.showerror('Error','npm tidak terdaftar')
        
    

class view():
    def __init__(self, nama, npm, alamat):
        self.master = Tk()
        self.master.title('Sistem Rekomendasi Profesi')
        self.master.geometry('840x520')
        self.master.resizable(width='FALSE',height='FALSE')
        #self.master.iconbitmap('icon.ico')
        self.nama = StringVar()
        self.npm = StringVar()
        self.alamat = StringVar()
        self.prof = StringVar()
        self.kdprof = StringVar()

        self.nama.set(nama)
        self.npm.set(npm)
        self.alamat.set(alamat)


        self.db_name = 'skripsi.db'

 
        Label(self.master, text = 'Nama', font = ('','10')).grid(row = 0, column = 0, sticky = W)
        Label(self.master, text = ': '+self.nama.get(), font = ('','10')).grid(row = 0, column = 1, sticky = W)
        #Label(self.master, text = self.nama.get()).grid(row = 0, column = 2, sticky = W)
        Label(self.master, text = 'NPM', font = ('','10')).grid(row = 1, column = 0, sticky = W)
        Label(self.master, text = ': '+self.npm.get(), font = ('','10')).grid(row = 1, column = 1, sticky = W)
        #Label(self.master, text = self.npm.get(), font = ('','10')).grid(row = 1, column = 2, sticky = W)
        Label(self.master, text = 'Alamat', font = ('','10')).grid(row = 2, column = 0, sticky = W)
        Label(self.master, text = ': '+self.alamat.get(), font = ('','10'), wraplength = 350, justify = LEFT).grid(row = 2, column = 1, sticky = W)
        #Label(self.master, text = self.alamat.get(), font = ('','10'), wraplength = 350, justify = LEFT).grid(row = 2, column = 2, sticky = W)
        Label(self.master, text = 'Jumlah SKS', font = ('','10')).grid(row  = 0, column = 3, sticky = E)
        #Label(self.master, text = ':', font = ('','10')).grid(row = 0, column = 4, sticky = W)
        #looping total hasil sks
        Label(self.master, text = 'Semester', font = ('','10')).grid(row = 1, column = 3, sticky =E)
        #Label(self.master, text = ':', font = ('','10')).grid(row = 1, column = 4, sticky = W)
        #if total sks blablabla
                
        Button(self.master, text = "Cek Profesi", font = ('','10'), command = self.viewing_records1, width = 10).grid(pady = 10, row = 3, column = 4, sticky =W)
        Button(self.master, text = "Result", font = ('','10'), command = self.viewing_records2, width = 10).grid(pady = 10, row = 3, column = 5, sticky =W)

        Label(self.master, text = 'Mata Kuliah yang Telah Diambil :', font = ('','10')).grid(row = 5, column = 1, columnspan = 2, sticky = W)
        self.tree = ttk.Treeview (height = 15)
        self.tree.grid (row = 10, column = 1, rowspan = 10, columnspan = 2, sticky = W)
        self.tree['columns'] = ('kdmatkul', 'matkul', 'sks')
        self.tree.heading ('#0', text = 'No', anchor = 'center')
        self.tree.column('#0', stretch = NO, width = 35, anchor = 'center')
        self.tree.heading ('kdmatkul', text = 'Kode Matakuliah', anchor = 'center')
        self.tree.column('kdmatkul', width = 100, anchor = 'center')
        self.tree.heading ('matkul', text = 'Mata Kuliah', anchor = 'center')
        self.tree.column('matkul', width = 230, anchor = 'center')
        self.tree.heading ('sks', text = 'SKS', anchor = 'center')
        self.tree.column('sks', width = 27, anchor = 'center')

        Label(self.master, text = 'Mata Kuliah yang Diperlukan Untuk Profesi', font = ('','10'),justify = LEFT).grid(padx = 25, row = 4, column = 3, columnspan = 2, sticky = W)
        self.tree1 = ttk.Treeview (height = 6)
        self.tree1.grid (padx = 25, row = 10, column = 3, columnspan = 3, sticky = E)
        self.tree1['columns'] = ('kdmatkul', 'matkul', 'sks')
        self.tree1.heading ('#0', text = 'No', anchor = 'center')
        self.tree1.column('#0', stretch = NO, width = 35, anchor = 'center')
        self.tree1.heading ('kdmatkul', text = 'Kode Matakuliah', anchor = 'center')
        self.tree1.column('kdmatkul', width = 80, anchor = 'center')
        self.tree1.heading ('matkul', text = 'Mata Kuliah', anchor = 'center')
        self.tree1.column('matkul', width = 210, anchor = 'center')        
        self.tree1.heading ('sks', text = 'SKS', anchor = 'center')
        self.tree1.column('sks', width = 27, anchor = 'center')

        Label(self.master, text = 'Mata Kuliah yang Harus Diambil', font = ('','10'),justify = LEFT).grid(padx = 25, row = 12, column = 3, columnspan = 2, sticky = W)
        self.tree2 = ttk.Treeview (height = 6)
        self.tree2.grid (padx = 25, row = 13, column = 3, columnspan = 3, sticky = E)
        self.tree2['columns'] = ('kdmatkul', 'matkul', 'sks')
        self.tree2.heading ('#0', text = 'No', anchor = 'center')
        self.tree2.column('#0', stretch = NO, width = 35, anchor = 'center')
        self.tree2.heading ('kdmatkul', text = 'Kode Matakuliah', anchor = 'center')
        self.tree2.column('kdmatkul', width = 80, anchor = 'center')
        self.tree2.heading ('matkul', text = 'Mata Kuliah', anchor = 'center')
        self.tree2.column('matkul', width = 210, anchor = 'center')        
        self.tree2.heading ('sks', text = 'SKS', anchor = 'center')
        self.tree2.column('sks', width = 27, anchor = 'center')

        self.sbl = Scrollbar(self.master)
        self.sbl.place(x=32, y=172, height=340)

        self.tree.configure(yscrollcommand = self.sbl.set)
        self.sbl.configure(command = self.tree.yview)

        self.viewing_records()


    def run_query (self, query, parameters = ()):
        with sqlite3.connect (self.db_name) as conn:
            cursor = conn.cursor()
            cursor.execute(query,parameters)
            query_result = cursor.fetchall()
            conn.commit ()
            return query_result

    def viewing_records (self):
        records = self.tree.get_children()
        for element in records:
            self.tree.delete (element)
        query = 'SELECT kdmatkul, nama_matkul, sks_matkul FROM matkul WHERE kdmatkul IN (SELECT kdmatkul FROM matkul_sec WHERE npm = ?) ORDER BY semester'
        parameters = (self.npm.get(),)
        db_rows = self.run_query (query, parameters)

        records2 = self.tree2.get_children()
        for element2 in records2:
            self.tree2.delete (element2)
        query2 = 'SELECT kdmatkul, nama_matkul, sks_matkul FROM matkul WHERE kdmatkul IN (SELECT kdmatkul FROM matkul_sec WHERE npm = ?) ORDER BY semester'
        parameters2 = (self.npm.get(),)
        db_rows = self.run_query (query2, parameters2)


        sks=0
        i = 1
        for row in db_rows:
            self.tree.insert ('', END, text = i, values = (row[0],row[1],row[2]))
            sks = sks + row[2]
            i += 1

        Label(self.master, text = ': {} sks'.format(sks), font = ('','10')).grid(row = 0, column = 4, sticky = W)

        if sks <= 18:
            Label(self.master, text = ': Semester 1', font = ('','10')).grid(row = 1, column = 4, sticky = W)
        elif sks > 18 and sks <= 37:
            Label(self.master, text = ': Semester 2', font = ('','10')).grid(row = 1, column = 4, sticky = W)
        elif sks > 37 and sks <= 56:
            Label(self.master, text = ': Semester 3', font = ('','10')).grid(row = 1, column = 4, sticky = W)
        elif sks > 56 and sks <= 76:
            Label(self.master, text = ': Semester 4', font = ('','10')).grid(row = 1, column = 4, sticky = W)
        elif sks > 76 and sks <= 96:
            Label(self.master, text = ': Semester 5', font = ('','10')).grid(row = 1, column = 4, sticky = W)
        elif sks > 96 and sks <= 116:
            Label(self.master, text = ': Semester 6', font = ('','10')).grid(row = 1, column = 4, sticky = W)
        elif sks > 116 and sks <= 134:
            Label(self.master, text = ': Semester 7', font = ('','10')).grid(row = 1, column = 4, sticky = W)
        elif sks > 134 and sks <= 154:
            Label(self.master, text = ': Semester 8', font = ('','10')).grid(row = 1, column = 4, sticky = W)

        Label(self.master, text = 'Silahkan Pilih Profesi :', font =('', '10')).grid(pady = 10, row =3, column = 1, sticky = E)
        self.Combobox = ttk.Combobox(width = 45, textvariable = self.prof, state = 'readonly')
        self.Combobox.grid (pady = 10,row = 3, column = 2, columnspan = 2, sticky = E)
        self.Combobox['values'] = self.combo_input()

    def combo_input(self):
        cursor = sqlite3.connect('skripsi.db').cursor()

        cursor.execute('SELECT * FROM profesi order by nama_profesi')

        data = []

        for row in cursor.fetchall():
            data.append(row[1])
            #data.append(row[0])
            #kdprof = row[0]
            #self.kdprof.set(kdprof)

        return data

    def viewing_records1 (self):
        records = self.tree1.get_children()
        for element in records:
            self.tree1.delete (element)
        with sqlite3.connect('skripsi.db') as db:
            c = db.cursor()
        if self.prof.get()=='':
            ms.showerror('Mohon Maaf','Silahkan Pilih Profesi Terlebih Dahulu')
            return;
        query = 'SELECT kdmatkul, nama_matkul, sks_matkul FROM matkul WHERE kdmatkul IN (SELECT kdmatkul FROM profesi_sec WHERE nama_profesi = ?) ORDER BY semester'
        parameters = (self.prof.get(),)
        #print(self.prof.get())
        #1print(self.kdprof.get())
        c.execute(query, parameters)
        entry = c.fetchone()


        if entry is None:

            # dataset dictionary SKKNI dan SAP
            data_profesi = {'DATA MODEL ADMINISTRATOR': 'data/uk_dma.csv',
                          'SENIOR SYSTEMS ANALYST': 'data/uk_ssa.csv',
                          'DATA ARCHITECT': 'data/uk_da.csv',
                          'DATABASE ADMINISTRATOR': 'data/uk_dba.csv',
                          'SENIOR OPERATIONS ANALYST': 'data/uk_soa.csv',
                          'PEMROGRAM KEPALA (LEAD PROGRAMMER)': 'data/uk_pk.csv',
                          'ANALIS PROGRAM (PROGRAM ANALYST)': 'data/uk_ap.csv',
                          'PENGEMBANG WEB (WEB DEVELOPER)': 'data/uk_pw.csv',
                          'SOFTWARE ENGINEER': 'data/uk_se.csv',
                          #'DIGITAL COMPUTER TECHNOLOGY ADVISOR': 'data/uk_dcta.csv',
                          'NETWORK SECURITY ANALYST': 'data/uk_nsa.csv',
                          'NETWORK ADMINISTRATOR': 'data/uk_na.csv',
                          'SYSTEM ADMINISTRATOR': 'data/uk_sad.csv',
                          'NETWORK DESIGNER': 'data/uk_nd.csv',
                          'SYSTEM ANALYST': 'data/uk_san.csv',
                          'ICTPM DEPUTY MANAGER': 'data/uk_idm.csv',
                          'DEPUTY MANAGER ICT PROJECT MANAGEMENT': 'data/uk_dmipm.csv',
                          'ENTERPRISE ARCHITECT': 'data/uk_ea.csv',
                          'CYBER SECURITY ANALYST': 'data/uk_csa.csv',
                          'IT AUDITOR': 'data/uk_ia.csv',
                          'IT AUDITOR MADYA TEKNOLOGI INFORMASI': 'data/uk_iamti.csv',
                          'PENGEMBANG CLOUD COMPUTING (CLOUD COMPUTING DEVELOPER)': 'data/uk_pcc.csv',
                          'MOBILE COMPUTING UTAMA (ADVANCE MOBILE COMPUTING)': 'data/uk_mcu.csv',
                          'WEB ANALYST': 'data/uk_wa.csv',
                          'ERP ANALYST': 'data/uk_ean.csv',
                          'ENTERPRISE RESOURCE PLANNING SECURITY ANALYST': 'data/uk_erpsa.csv',
                          'ENTERPRISE RESOURCE PLANNING DATA ARCHITECT': 'data/uk_erpda.csv',
                          'ENTERPRISE RESOURCE PLANNING (ERP) INFRASTRUCTURE ADMINISTRATOR': 'data/uk_erpia.csv',
                          'ENTERPRISE RESOURCE PLANNING MASTER DATA ANALYST': 'data/uk_erpmda.csv'
                          }

            data_rps = {#'data/rps_ap1.csv': ('IT-011302', 'Algoritma & Pemrograman 1','3'),
                        #'data/rps_ap2.csv': ('IT-011303', 'Algoritma & Pemrograman 2','3'),
                        #'data/rps_apsi.csv': ('AK-011302', 'Analisis dan Perancangan Sistem Informasi','3'),
                        #'data/rps_aks.csv': ('AK-011303', 'Analisis Kinerja Sistem','3'),
                        'data/rps_jarkom.csv':('AK-011331', 'Desain dan Manajemen Jaringan Komputer','3'),
                        #'data/rps_etika.csv': ('PB-011206', 'Etika dan Profesionalisme Tek. Sistem Inf.','2'),
                        #'data/rps_graf.csv': ('IT-011308', 'Graf dan Analasis Algoritma','3'),
                        #'data/rps_grafkom.csv': ('AK-011204', 'Grafik Komputer dan Pengolahan Citra','2'),
                        'data/rps_imk.csv': ('AK-011305', 'Interaksi Manusia dan Komputer','3'),
                        #'data/rps_kdm.csv': ('IT-011234', 'Konsep Data Mining','2'),
                        #'data/rps_ksi.csv': ('IT-011309', 'Konsep Sistem Informasi','3'),
                        #'data/rps_ksil.csv': ('AK-011208', 'KONSEP SISTEM INFORMASI LANJUT'),
                        #'data/rps_pbw.csv': ('AK-011211', 'Pemrograman Berbasis Web','2'),
                        #'data/rps_pbo.csv': ('AK-011312', 'Pemrograman Berorientasi Objek','3'),
                        #'data/rps_pbti.csv': ('PP-011201', 'Pengantar Bisnis Teknologi Informasi','2'),
                        #'data/rps_poak.csv': ('IT-011217', 'Pengantar Organisasi & Arsitektur Komp.','2'),
                        #'data/rps_ptkom.csv': ('IT-011318', 'Pengantar Teknik Kompilasi','3'),
                        #'data/rps_ptsi.csv': ('IT-011416', 'Peng. Teknologi Sistem Informasi','4'),
                        #'data/rps_ppsi.csv': ('AK-011315', 'Pengelolaan Proyek Sistem Informasi','3'),
                        #'data/rps_sbd1.csv': ('AK-011317', 'Sistem Basis Data 1','3'),
                        #'data/rps_sbd2.csv': ('AK-011318', 'Sistem Basis Data 2','3'),
                        #'data/rps_sbdl.csv': ('AK-011219', 'SISTEM BASIS DATA LANJUT'),
                        #'data/rps_sbp.csv': ('IT-011222', 'Sistem Berbasis Pengetahuan','2'),
                        #'data/rps_sig.csv': ('AK-011220', 'Sistem Informasi Geografis','2'),
                        #'data/rps_sip.csv': ('AK-011222', 'Sistem Informasi Perbankan','2'),
                        #'data/rps_keamanan.csv': ('AK-011332', 'Sistem Keamanan Teknologi Informasi','3'),
                        'data/rps_so.csv': ('IT-011325', 'Sistem Operasi','3'),
                        #'data/rps_spk.csv': ('AK-011224', 'Sistem Penunjang Keputusan','2'),
                        #'data/rps_sister.csv': ('AK-011225', 'Sistem Terdistribusi','2'),
                        #'data/rps_sod1.csv': ('IT-011228', 'Struktur Organisasi Data 1','2'),
                        #'data/rps_sod2.csv': ('IT-011229', 'Struktur Organisasi Data 2','2'),
                        #'data/rps_tpt1.csv': ('IT-011230', 'Teknik Pemrograman Terstruktur 1','2'),
                        #'data/rps_tpt2.csv': ('IT-011231', 'Teknik Pemrograman Terstruktur 2','2'),
                        #'data/rps_tou1.csv': ('IT-011232', 'Teori Organisasi Umum 1','2'),
                        #'data/rps_testing.csv': ('AK-011326', 'Testing Dan Implementasi Sistem','3')
                        }

            # Profesi
            for profesi, dataset_profesi in data_profesi.items(): # profesi, dataset_profesi adalah penamaan key dan value dict data_profesi
                if self.prof.get() == profesi: # conditional bila nama profesi yang ada (atribut nama_profesi) sama dengan data nama profesi yang ada di dict data_skkni
                    with open(dataset_profesi, 'r', encoding="utf-8") as csv_file: # dataset_profesi adalah penamaan value dict data_profesi
                        csv_reader = csv.reader(csv_file)
                        next(csv_reader) # skip baris kolom pertama

                        list_kw_profesi = [] # daftar kata penting profesi dalam bentuk list
                        for row in csv_file:
                            usenorm = normalize()
                            text_norm = usenorm.tokenize(row)
                            list_kw_profesi.extend(text_norm)
                        keyword_profesi = Counter(list_kw_profesi) # menghitung jumlah masing-masing kata penting profesi


            # RPS
            profesi_matkul = [] # mapping mata kuliah yang dibutuhkan profesi dalam bentuk list
            # memeriksa dari dictionary dataset RPS
            for dataset_rps, matkul in data_rps.items(): # dataset_rps, matkul adalah penamaan key dan value dict data_rps
                with open(dataset_rps, 'r', encoding="utf-8") as csv_file:
                    csv_reader = csv.reader(csv_file)
                    next(csv_reader) # skip baris kolom pertama

                    list_kw_rps = [] # daftar kata penting rps dalam bentuk list
                    for row in csv_file:
                        # preprocessing
                        usenorm = normalize()
                        text_norm = usenorm.tokenize(row)
                        list_kw_rps.extend(text_norm)
                    keyword_rps = Counter(list_kw_rps) # menghitung jumlah masing-masing kata penting rps

                    # menghitung similarity kata penting profesi dan rps
                    def jaccard_similarity(x,y):
                        intersection_cardinality = len(set.intersection(*[set(x), set(y)]))
                        union_cardinality = len(set.union(*[set(x), set(y)]))
                        return intersection_cardinality/float(union_cardinality)

                    hasil = jaccard_similarity(list_kw_profesi, list_kw_rps)
                    hasil_percent = '{0:.0%}'.format(hasil)
                    kata_sama = set.intersection(*[set(list_kw_profesi), set(list_kw_rps)])

                    print('>>  Hasil similarity profesi ' +self.prof.get()+ ' dan  matkul ' +matkul[1]+ ' adalah...')
                    print('    ' +hasil_percent+ '\n')

                # conditional penentuan mata kuliah dengan profesi
                if hasil > 0 : # conditional jika memiliki nilai kemiripan, mata kuliah tersebut dibutuhkan pada profesi yang bersangkutan
                    profesi_matkul.append(matkul) # menambahkan data matkul dari dict ke list profesi_matkul
                    # menyimpan hasil log output tokenizing
                    #output = open('log.txt', 'a')
                    #output.write
                    print('>> Daftar kata penting untuk profesi ' + self.prof.get() + '\n')
                    #output.write
                    print(str(list_kw_profesi)+ '\n\n')
                    #output.write
                    print('>> Daftar kata penting di RPS mata kuliah ' +matkul[1]+ '\n')
                    #output.write
                    print(str(list_kw_rps)+ '\n\n')
                    #output.write
                    print('>> Jumlah masing-masing kata penting untuk profesi ' +self.prof.get()+ '\n')
                    #output.write
                    print(str(len(keyword_profesi))+ '\n\n')
                    #output.write
                    print('>> Jumlah masing-masing kata penting di RPS mata kuliah ' +matkul[1]+ '\n')
                    #output.write
                    print(str(len(keyword_rps))+ '\n\n')
                    #output.write
                    print('>> Hasil similarity profesi ' +self.prof.get()+ ' dan SAP matkul ' +matkul[1]+ ' adalah...\n')
                    #output.write
                    print('('+str(len(kata_sama))+' / ('+str(len(keyword_rps))+' + '+str(len(keyword_profesi))+' - '+str(len(kata_sama))+'))* 100% = '+hasil_percent+ '\n\n')
                    
                    #output.write
                    print('>> Kode Unit yang sama di profesi ' +self.prof.get()+ ' dan SAP matkul ' +matkul[1]+ ':\n')
                    #output.write
                    print(str(kata_sama)+ '\n\n')
                    
                    #output.write
                    print('#####\n\n')
                    #output.close()


            # menambahkan data matkul dari list profesi_matkul ke database
            no = 1
            for i in profesi_matkul:
                with sqlite3.connect('skripsi.db') as db:
                    c = db.cursor()
                c.execute("INSERT OR IGNORE INTO profesi_sec (nama_profesi, kdmatkul) VALUES(?, ?)", (self.prof.get(), i[0]) )
                db.commit()
                self.tree1.insert('', END, text = no, values=((i[0]),(i[1]),(i[2])))
                no += 1


        # bila data matkul yang dibutuhkan profesi bersangkutan ditemukan dalam database
        else:
            print('\nData untuk profesi ' + self.prof.get() + ' ditemukan!\n')
            records = self.tree1.get_children()
            for element in records:
            	self.tree1.delete(element)
            query = ('SELECT matkul.kdmatkul, matkul.nama_matkul, matkul.sks_matkul, profesi_sec.kdmatkul FROM matkul, profesi_sec WHERE matkul.kdmatkul = profesi_sec.kdmatkul AND profesi_sec.nama_profesi = ?')
            parameters = (self.prof.get(),)
            db_rows = self.run_query(query, parameters)
            
            i = 1
            for row in db_rows:
                self.tree1.insert('', END, text = i, values=(row[0],row[1],row[2]))
                i += 1

            print('Data ditampilkan ke table treeview.\n')
        Label(self.master, text = '{} :'.format(self.prof.get()), font = ('','10'),justify = LEFT).grid(padx = 25, row = 5, column = 3, columnspan = 2, sticky = W)

    def viewing_records2 (self):
        records = self.tree2.get_children()
        for element in records:
            self.tree2.delete (element)
        query = 'SELECT matkul.kdmatkul, matkul.nama_matkul, matkul.sks_matkul, profesi_sec.kdmatkul, profesi_sec.nama_profesi FROM matkul, profesi_sec WHERE matkul.kdmatkul = profesi_sec.kdmatkul AND profesi_sec.kdmatkul NOT IN (SELECT matkul_sec.kdmatkul FROM matkul_sec WHERE matkul_sec.npm = ?) AND profesi_sec.nama_profesi = ? ORDER BY matkul.kdmatkul'
        parameters = (self.npm.get(),self.prof.get(),)
        #print('('+self.prof.get()+')')
        #1print(self.kdprof.get())
        db_rows = self.run_query (query, parameters)
        if self.prof.get()=='':
            ms.showerror('Mohon Maaf','Silahkan Pilih Profesi Terlebih Dahulu')
            return;

        k = 1
        for row in db_rows:
            self.tree2.insert ('', END, text = k, values = (row[0],row[1],row[2]))
            k += 1
        if k > 1:
            ms.showerror('Mohon Maaf','Silahkan Terlebih Dahulu Mengambil Mata Kuliah\nBerikut')
        else:
            ms.showinfo('Congrats',self.nama.get() + '\nSudah Memenuhi Persyaratan \nProfesi ' + self.prof.get())
    



if __name__ == '__main__':
    root=Tk()
    application=Login(root)
    root.geometry('450x160')
    root.iconbitmap(default = 'icon.ico')
    root.mainloop()